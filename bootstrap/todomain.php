<?php
require 'db_conn.php';
require 'config.php';
session_start(); 
   
//redirect if user has yet to login
if (!isset($_SESSION['id'])) { 
    $_SESSION['msg'] = "You have to log in first"; 
    header('location: login.php'); 
} 
   
//logout



$var= (int)$_SESSION['id'];


?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset= "UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge"> 
<link rel="stylesheet" href="css/bootstrap-grid.min.css">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="style.css">

<style>  
.welcomes { 
                float:right;
               
                position:right;
                padding-right: 20px;
                
            } 
  
            
        </style>  



<form action="logout.php" method="GET" autocomplete="off">
<div class="welcomes">
  <?php if (isset($_SESSION['success'])) : ?> 
            
                <h5> 
                    <?php
                        echo $_SESSION['success'];  
                        
                        echo $_SESSION["username"]
                    ?> 
                </h5> 
            
        <?php endif ?> 
<button type="submit" class="btn btn-primary logout">Logout</button>


 
  </form>
</div>

<title>TODO App</title>
<script src="https://kit.fontawesome.com/d72928d9b9.js" crossorigin="anonymous"></script>
</head>
<body>
	<div class="container m-5 p-2 rounded mx-auto bg-light shadow">
    <!-- App title section -->
    <div class="row m-1 p-4">
        <div class="col">
            <div class="p-1 h1 text-primary text-center mx-auto display-inline-block">
                <i class="fa fa-check bg-primary text-white rounded p-2"></i>
                <u>My Todo-s</u>
            </div>
        </div>
    </div>
    <!-- Create todo section -->
  <form action="app/add.php" method="POST" autocomplete="off">
    <div class="row m-1 p-3">
        <div class="col col-11 mx-auto">
            
            <div class="row bg-white rounded shadow-sm p-2 add-todo-wrapper align-items-center justify-content-center">
                
                <div class="col">
                    <input class="form-control form-control-lg border-0 add-todo-input bg-transparent rounded" type="text" name="title" placeholder="Add new ..">
                </div>
                <div class="col-auto m-0 px-2 d-flex align-items-center">
                    <label class="text-secondary my-2 p-0 px-1 view-opt-label due-date-label d-none">Due date not set</label>
                    <i class="fa fa-calendar my-2 px-1 text-primary btn due-date-button" data-toggle="tooltip" data-placement="bottom" title="Set a Due date"></i>
                    <i class="fa fa-calendar-times-o my-2 px-1 text-danger btn clear-due-date-button d-none" data-toggle="tooltip" data-placement="bottom" title="Clear Due date"></i>
                </div>
                
                <div class="col-auto px-0 mx-0 mr-2">
                    
                    <button type="submit" class="btn btn-primary">Add</button>
                    
                    
                </div>
             
            </div>

            
        </div>
    </div>
    </form>
        
     
          <?php
            $todos=$conn->query("SELECT * FROM  todos  WHERE user_id=$var ORDER BY id ASC");
          ?>

 
  <div class="row mx-1 px-5 pb-3 w-80">
        <div class="col mx-auto">

           <?php
           while($todo=$todos->fetch(PDO::FETCH_ASSOC)){ ?>
            

            
            <div class="row px-3 align-items-center todo-item rounded">

                <?php if($todo['checked']){ ?>
                
                <input type="checkbox" class="box" todo-data="<?php echo $todo['id'];?>" checked />
            <h2 class="checked"><?php echo $todo['title'] ?> </h2>
            <div class="col-auto m-1 p-0 todo-actions">
                    <div class="row d-flex align-items-center justify-content-end">
              
            </div>
          </div>
        
             

                      
            <?php } else{ ?>
                <input type="checkbox" class="box" todo-data="<?php echo $todo['id'];?>"/>
                 <h2><?php echo $todo['title'] ?></h2>
                   
                          

            <?php } ?>

                <div class="col-auto m-1 p-0 d-flex align-items-center">


                    <h2 class="m-0 p-0">
                        <i class="fa fa-square-o text-primary btn m-0 p-0 d-none" data-toggle="tooltip" data-placement="bottom" title="Mark as complete"></i>
                        
                    </h2>
                </div>
               
                
             
                <div class="col-auto m-1 p-0 todo-actions">
                    <div class="row d-flex align-items-center justify-content-end">

                        <h5 class="m-0 p-0 px-2">
                            <i class="fa fa-pencil text-info btn m-0 p-0" data-toggle="tooltip" data-placement="bottom" title="Edit todo"></i>
                        </h5>
                        <h5 class="m-0 p-0 px-2">
                          
                             
                        <i class="todo-removal fa fa-trash-o text-danger btn m-0 p-0" data-toggle="tooltip" data-placement="bottom" title="Delete todo" id="<?php echo $todo['id']; ?>"></i>
                      </h5> 
                    </div>
                    <div class="row todo-created-info">
                        <div class="col-auto d-flex align-items-center pr-2">
                            <i class="fa fa-info-circle my-2 px-2 text-black-50 btn" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Created date"></i>
                            <label class="date-label my-2 text-black-50"><?php echo $todo['date_time'] ?></label>
                        </div>
                    </div>
                </div>
            </div>
        
        <?php } ?>

    



</div>
 

 <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/myjs.js" ></script>
    <script src="js/jquery-3.3.1.min.js"></script>

    <script>
        $(document).ready(function(){

          $(".todo-removal").click(function(e){
                const id = $(this).attr('id');
                 $.post('app/remove.php', 
                      {
                          id: id
                      },
                      (data) => {
                         if(data){
                            $(this).parent().parent().parent().parent().hide();
                         }
                      }

                );
                
               
            });
          
 



            $(".box").click(function(e){
                const id = $(this).attr('todo-data');
                
                $.post('app/check.php', 
                      {
                          id: id
                      },
                      (data) => {
                         if(data!='error'){
                            const h2= $(this).next();
                            if(data === '1'){
                                h2.removeClass('checked');
                            }else{
                                h2.addclass('checked');
                            }
                         }
                      }

                );
            });
        });
    </script>
 

</body>
</html>